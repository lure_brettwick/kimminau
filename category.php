<?php get_header(); ?>

<!-- category.php -->

<?php get_template_part('template-part', 'head'); ?>
<?php get_template_part('template-part', 'topnav'); ?>
<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	$image = $image[0]; ?>
	<?php else :
	$image = get_bloginfo( 'stylesheet_directory') . '/img/default-header-bg.jpg'; ?>
	<?php endif; ?>
<div id="header-bg" class="row" style="background-image: url('<?php echo $image; ?>')">
	<div class="inner">
		 <h1 class="page-header"><?php printf( __( 'Category Archives: %s', 'twentythirteen' ), single_cat_title( '', false ) ); ?></h1>
	</div><!--end inner-->
</div><!--end header-bg-->
<div class="border"></div><!--end border-->
<div class="row dmbs-content">
   <div class="inner">
	<div id="content" class="col-xs-12 col-sm-9">
			<?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post
                    if ( is_single() ) : ?>

                        <div <?php post_class(); ?>>

                            <h2 class="page-header"><?php the_title() ;?></h2>

                            <?php // if ( has_post_thumbnail() ) : ?>
                                <?php // the_post_thumbnail(); ?>
                                <!-- <div class="clear"></div> -->
                            <?php // endif; ?>
                            <?php the_excerpt(); ?>
                            <?php wp_link_pages(); ?>
                            <?php get_template_part('template-part', 'postmeta'); ?>
                            

                        </div>
                    <?php
                    // list of posts
                    else : ?>
                       <div <?php post_class(); ?>>

                            <h2 class="page-header">
                                <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h2>

                            <?php // if ( has_post_thumbnail() ) : ?>
                                <?php // the_post_thumbnail(); ?>
                                <!-- <div class="clear"></div> -->
                            <?php // endif; ?>
                            <?php the_excerpt(); ?>
                            <?php wp_link_pages(); ?>
                            <?php get_template_part('template-part', 'postmeta'); ?>
                            
                       </div>

                     <?php  endif; ?>

                <?php endwhile; ?>
                <div class="navigation">
                  <ul id="post-prev-next" class="list-inline">
                    <li><?php previous_posts_link( '&laquo; Previous Entries' ); ?></li>
                    <li><?php next_posts_link( 'Next Entries &raquo;', '' ); ?></li>
                  </ul>
                </div>
                <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>
	</div><!--end content-->
	<div id="right-sidebar" class="col-xs-12 col-sm-3">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Right Sidebar") ) : ?><?php endif; ?>
	</div><!--end home-featured-->
   </div><!--end inner-->
</div>
<?php get_footer(); ?>