<?php get_header(); ?>
<?php get_template_part('template-part', 'head'); ?>
<?php get_template_part('template-part', 'topnav'); ?>
<!-- start content container -->
<!-- start content container -->
<?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	$image = $image[0]; ?>
	<?php else :
	$image = get_bloginfo( 'stylesheet_directory') . '/img/default-header-bg.jpg'; ?>
	<?php endif; ?>
<div id="header-bg" class="row" style="background-image: url('<?php echo $image; ?>')">
	<div class="inner">
		<h1 class="page-header"><?php the_title() ;?></h1>
	</div><!--end inner-->
</div><!--end header-bg-->
<div class="border"></div><!--end border-->
<div class="row dmbs-content">
   <div class="inner">
	<div id="content" class="col-xs-12 col-sm-12">
        <?php the_content(); ?>
        <?php endwhile; ?>
        <?php else: ?>
            <?php get_404_template(); ?>
        <?php endif; ?>
	</div><!--end content-->
   </div><!--end inner-->
</div>
<!-- end content container -->
<?php if(is_page(806)){ ?>
<div id="our-mission">
	<div class="inner">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Our Mission") ) : ?><?php endif; ?>
	</div><!--end inner-->
</div><!--end our-mission-->
<?php } ?>
<div id="gallery-section">
	<div class="border"></div><!--end funnel-border-->
		<div id="desktop-gallery">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home - Gallery") ) : ?><?php endif; ?>
		</div><!--end desktop-gallery-->
		<div id="mobile-gallery">
			<a href="/gallery/" class="button">Wood Floor Gallery</a>
		</div><!--end mobile-gallery-->
	<div class="border"></div><!--end funnel-border-->
</div><!--end gallery-section-->
<div id="contact-form" class="row">
	<div class="inner">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Contact Form") ) : ?><?php endif; ?>
	</div><!--end inner-->
</div><!--end contact-form-->
<div class="border"></div><!--end funnel-border-->
<div id="testimonials" class="row secondary">
	<div class="inner">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Testimonials") ) : ?><?php endif; ?>
			<a href="/testimonials/" class="button">More Testimonials</a>
	</div><!--end inner-->
</div><!--end testimonials-->
<?php get_footer(); ?>
