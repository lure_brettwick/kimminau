<?php

/*
Template Name: Full-Width Page
*/
?>

<?php get_header(); ?>
<?php get_template_part('template-part', 'head'); ?>
<?php get_template_part('template-part', 'topnav'); ?>
<!-- start content container -->
<!-- start content container -->
<?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	$image = $image[0]; ?>
	<?php else :
	$image = get_bloginfo( 'stylesheet_directory') . '/img/default-header-bg.jpg'; ?>
	<?php endif; ?>
<div id="header-bg" class="tpl-full-width-header row" style="background-image: url('<?php echo $image; ?>')">
	<?php if (is_active_sidebar('tpl-full-width-video-header')) : ?>
		<div class="video-header">
			<?php dynamic_sidebar('tpl-full-width-video-header'); ?>
		</div>
	<?php endif; ?>
	<div class="container inner">
		<div class="row">
			<div class="col-md-9">
				<h1 class="page-header"><?php the_title() ;?></h1>
			</div>
		</div>
			<?php if (is_active_sidebar('tpl-full-width-top')) : ?>
				<div class="tpl-full-width-header__action">
					<?php dynamic_sidebar('tpl-full-width-top'); ?>
				</div>
			<?php endif; ?>
		</div>
	</div><!--end inner-->
</div><!--end header-bg-->
<div class="border"></div><!--end border-->
<div class="row dmbs-content">
  <div class="inner">
		<div id="content">
      <?php the_content(); ?>
      <?php endwhile; ?>
      <?php else: ?>
        <?php get_404_template(); ?>
      <?php endif; ?>

			<?php if (is_active_sidebar('tpl-full-width-bottom')) : ?>
				<?php dynamic_sidebar('tpl-full-width-bottom'); ?>
			<?php endif; ?>

		</div><!--end content-->
  </div><!--end inner-->
</div>
<?php get_footer(); ?>
