<?php 

/*
Template Name: Home Page
*/

get_header(); ?>

<?php get_template_part('template-part', 'head'); ?>

<div id="hero">
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home - Hero") ) : ?><?php endif; ?>
	<div id="caption-row" class="row">
	<div class="col-xs-12 col-sm-4"></div>
	<div id="hero-caption" class="col-xs-12 col-sm-8">
		Quality In Every Step
		<a class="button" href="/gallery/">Wood Floor Gallery</a>
	</div><!--end col-sm-6-->
	</div><!--end row-->
</div><!--end hero-->
<div id="funnel-container" class="row">
	<div class="border"></div><!--end funnel-border-->
	<div class="inner">
		<div id="funnel">
			<div id="refinishing" class="col-xs-12 col-sm-4 service">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Funnel - Refinishing") ) : ?><?php endif; ?>
			</div><!--end refinishing-->
			<div id="maintenance" class="col-xs-12 col-sm-4 service">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Funnel - Maintenance") ) : ?><?php endif; ?>
			</div><!--end maintenance-->
			<div id="clean" class="col-xs-12 col-sm-4 service">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Funnel - Clean") ) : ?><?php endif; ?>
			</div><!--end clean-->
		</div><!--end funnel-->
		<div id="mobile-funnel">
			<h2>Kansas City’s Premier Flooring Company</h2>
			<a href="/floor-refinishing/">Floor Refinishing</a>
			<a href="/floor-maintenance/">Floor Maintenance</a>
			<a href="/clean-and-coat/">Clean &amp; Coat</a>
		</div><!--end mobile-funnel-->
	</div><!--end inner-->
</div><!--end funnel-container-->
<!-- start content container -->
<div class="row dmbs-content">
	<div id="home-featured" class="col-xs-12 col-sm-5">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home - Featured") ) : ?><?php endif; ?>
	</div><!--end home-featured-->
	<div id="content" class="col-xs-12 col-sm-7">
		<div class="inner">
		        <?php // theloop
          		if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            		<?php global $more; $more = FALSE; ?>
            		<h1 class="page-header"><?php the_title() ;?></h1>
            		<?php // the_content(); ?>
            		<?php the_content('Read more'); ?>
            		<?php // wp_link_pages(); ?>
            		<?php // comments_template(); ?>
        		<?php endwhile; ?>
        		<?php else: ?>
            		<?php get_404_template(); ?>
        		<?php endif; ?>
		</div><!--end inner-->
	</div><!--end content-->
</div>
<!-- end content container -->
<div id="gallery-section">
	<div class="border"></div><!--end funnel-border-->
		<div id="desktop-gallery">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home - Gallery") ) : ?><?php endif; ?>
		</div><!--end desktop-gallery-->
		<div id="mobile-gallery">
			<a href="/gallery/" class="button">Wood Floor Gallery</a>
		</div><!--end mobile-gallery-->
	<div class="border"></div><!--end funnel-border-->
</div><!--end gallery-section-->
<div id="testimonials" class="row">
	<div class="inner">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Testimonials") ) : ?><?php endif; ?>
			<a href="/testimonials/" class="button">More Testimonials</a>
	</div><!--end inner-->
</div><!--end testimonials-->
<div id="blog-feed-container" class="row">
		<div class="inner">
			<h2>Kimminau News &amp; Tips</h2>
			<div id="news">
            			<ul>
                			<?php $the_query = new WP_Query( 'showposts=2' ); ?>
                			<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                			<li class="col-xs-12 col-sm-6 blog-post">
						<?php if (has_post_thumbnail( $post->ID ) ): ?>
						<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
						$image = $image[0]; ?>
						<?php else :
						$image = get_bloginfo( 'stylesheet_directory') . '/img/default-feed-bg.jpg'; ?>
						<?php endif; ?>
						<div class="inner" style="background-image: url('<?php echo $image; ?>')">
                    					<div>
                        					<div class="shade">
                            						<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                        					</div>                    
                    					</div>
						</div><!--end inner-->
                			</li>
                			<?php endwhile;?>
            			</ul>
    			</div><!--end news-->
		</div><!--end inner-->
</div><!--end blog-feed-container-->
<?php get_footer(); ?>
