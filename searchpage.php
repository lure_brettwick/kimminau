<?php
/*
Template Name: Search Page
*/
?>
<?php get_header(); ?>
<?php get_template_part('template-part', 'head'); ?>
<?php get_template_part('template-part', 'topnav'); ?>
<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	$image = $image[0]; ?>
	<?php else :
	$image = get_bloginfo( 'stylesheet_directory') . '/img/default-header-bg.jpg'; ?>
	<?php endif; ?>
<div id="header-bg" class="row" style="background-image: url('<?php echo $image; ?>')">
	<div class="inner">
		 <h1 class="page-header"><?php the_title() ;?></h1>
	</div><!--end inner-->
</div><!--end header-bg-->
<div class="border"></div><!--end border-->
<div class="row dmbs-content">
   <div class="inner">
	<div id="content" class="col-xs-12 col-sm-9">
			 <?php get_search_form(); ?>
	</div><!--end content-->
	<div id="right-sidebar" class="col-xs-12 col-sm-3">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Right Sidebar") ) : ?><?php endif; ?>
	</div><!--end home-featured-->
   </div><!--end inner-->
</div>
<?php get_footer(); ?>