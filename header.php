<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
    <meta name="description" content="<?php echo esc_attr(get_bloginfo('description')); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <meta name="google-site-verification" content="EIompHp83RaDZvL6ML9owqvfHIB_5cfZx03sfNrddPo" />

<meta name="msvalidate.01" content="860846D1D208E70545144F66F3D55764" /> 
<script async src="//39144.tctm.co/t.js"></script>
<!-- Facebook Pixel Code -->

    <?php wp_head(); ?>
<!--Google Analytics-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-27529136-1', 'auto');
  ga('send', 'pageview');

</script>
<!--End Google Analytics-->
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/2939543.js"></script>
<!-- End of HubSpot Embed Code -->
</head>
<body <?php body_class(); ?>>
