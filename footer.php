<footer id="main-footer" class="row">
	<div class="border"></div><!--end border-->
	<div class="inner">
		<div id="footer-logo" class="col-xs-12 col-sm-3">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Logo") ) : ?><?php endif; ?>
			<div id="social-links">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Social Links") ) : ?><?php endif; ?>
			</div><!--end social-links-->
		</div><!--end footer-logo-->
		<div id="footer-cities" class="col-xs-12 col-sm-6">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Cities") ) : ?><?php endif; ?>
		</div><!--end footer-cities-->
		<div id="footer-right" class="col-xs-12 col-sm-3">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Right") ) : ?><?php endif; ?>
		</div><!--end footer-right-->
	</div><!--end inner-->
	<div id="copyright">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("copyright") ) : ?><?php endif; ?>
	</div><!--end copyright-->
</footer><!--end main-footer-->
<?php wp_footer(); ?>
<script>
// mobile button
jQuery(function(){
  jQuery('#nav').click(function() {
    jQuery(this).toggleClass('open');
  });
});

// header: off top scroll behavior 
//jQuery(document).ready(function( $ ) {

	// set variables	
	//var $document = $(document),
    //$element = $('header#main-header'),
   // className = 'offtop';

	//$document.scroll(function() {
	 // $element.toggleClass(className, $document.scrollTop() >= 50);
	//});
	
//});
</script>
</body>
</html>