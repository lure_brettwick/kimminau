<?php get_header(); ?>
<?php get_template_part('template-part', 'head'); ?>
<?php get_template_part('template-part', 'topnav'); ?>
<!-- start content container -->
<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	$image = $image[0]; ?>
	<?php else :
	$image = get_bloginfo( 'stylesheet_directory') . '/img/default-header-bg.jpg'; ?>
	<?php endif; ?>
<div id="header-bg" class="row" style="background-image: url('<?php echo $image; ?>')">
	<div class="inner">
		<h1 class="page-header"><?php _e('404 Page Not Found!','devdmbootstrap3'); ?></h1>
	</div><!--end inner-->
</div><!--end header-bg-->
<div class="border"></div><!--end border-->
<div class="row dmbs-content">
   <div class="inner">
	<div id="content" class="col-xs-12 col-sm-12">
		<p>The page you are trying to reach does not exist. Please try another page or return to the <a href="/">homepage</a></p>
        <br><br>
	</div><!--end content-->
   </div><!--end inner-->
</div>
<!-- end content container -->
<?php get_footer(); ?>
