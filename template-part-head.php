<header id="main-header">
	<div id="header-mobile">
		<div id="header-top" class="row">
			<div class="inner">
				<div id="header-phone" class="col-xs-12 col-sm-7">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Header Phone") ) : ?><?php endif; ?>
				</div><!--end header-phone-->
				<a class="button" id="mobile-phone" href="tel:8168753555">Call Now</a>
			</div><!--end inner-->
		</div><!--end header-top-->
		<div id="header-bottom" class="row">
			<div class="inner">
				<div id="navigation" class="col-xs-12 col-sm-9 nav-container">
					<nav id="nav">
        					<?php wp_nav_menu( array('menu' => 'Top Navigation' )); ?>
      					</nav>
				</div><!--end nav-container-->
			</div><!--end inner-->
		</div><!--end header-bottom-->
		<div class="border"></div><!--end header-border-->
	</div><!--end mobile-header-->
	<div id="logo-container" class="row">
		<div class="inner">
			<div id="header-logo" class="col-xs-12 col-sm-3">
				<a id="link-home" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="/wp-content/themes/kimminau/img/logo-header.png" width="100%" height="auto" alt="Kimminau Wood Floors Logo"></a>
			</div><!--end header-logo-->
		</div><!--end inner-->
	</div><!--end logo-container-->
</header><!--end main-header-->