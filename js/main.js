jQuery(document).ready(function(event) {
	jQuery(".scroll-to").on("click", function(event) {
		var target = jQuery(this).attr("href") || jQuery(this).data("target");
		targetEl = jQuery(target);

		if (targetEl !== "undefined" && targetEl.length > 0) {
			event.preventDefault();
			jQuery("html, body").animate({
				scrollTop: targetEl.offset().top - 90
			}, 1000);
		}
	});
});
