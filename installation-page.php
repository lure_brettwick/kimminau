<?php

/*
Template Name: Installation Page
*/

get_header(); ?>
<?php get_template_part('template-part', 'head'); ?>
<!-- start content container -->
<?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	$image = $image[0]; ?>
	<?php else :
	$image = get_bloginfo( 'stylesheet_directory') . '/img/default-header-bg.jpg'; ?>
	<?php endif; ?>
<div id="header-bg" class="row" style="background-image: url('<?php echo $image; ?>')">
	<div class="container inner">
    <div class="row">
      <div class="col-md-7">
        <h1 class="page-header"><?php the_title() ;?></h1>
      </div>
      <div class="col-md-5">
        <?php if (function_exists('dynamic_sidebar') && is_active_sidebar('custom_sidebar_3')) : ?>
        	<div class="page-header-caption bg-white-transparent">
        		<?php dynamic_sidebar('custom_sidebar_3'); ?>
          </div><!-- #custom-sidebar-3 -->
        <?php endif; ?>
      </div>
    </div><!-- .row -->
	</div><!--end inner-->
</div><!--end header-bg-->
<div class="border"></div><!--end border-->
<div class="row dmbs-content">
   <div class="inner">
	<div id="content" class="col-xs-12 col-sm-8">
        <?php the_content(); ?>
        <?php endwhile; ?>
        <?php else: ?>
            <?php get_404_template(); ?>
        <?php endif; ?>

	</div><!--end content-->
	<div id="right-sidebar" class="col-xs-12 col-sm-4">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Right Sidebar") ) : ?><?php endif; ?>
	</div><!--end right-sidebar-->
   </div><!--end inner-->
</div>

<!-- end content container -->

<!-- Gallery CTAs -->

<?php if (function_exists('dynamic_sidebar') && is_active_sidebar('custom_sidebar_1')) : ?>
	<section id="custom-sidebar-1" class="ins-galleries custom-sidebar-1 widget-area" role="complementary">
    <div class="container">
      <div class="row">
		    <?php dynamic_sidebar('custom_sidebar_1'); ?>
      </div><!-- .row -->
    </div>
	</section><!-- #custom-sidebar-1 -->
<?php endif; ?>

<!-- 7 Point Process -->

<section class="ins-points bg-img-wood-grain">
  <div class="border"></div><!-- end funnel-border -->
  <div class="container">
    <h2 class="ins-points-title text-serif text-gold text-shadow text-small-caps text-center">Our Seven Point Process</h2>

    <?php if (function_exists('dynamic_sidebar') && is_active_sidebar('custom_sidebar_2')) : ?>
      <div id="custom-sidebar-2" class="custom-sidebar-2 widget-area" role="complementary">
        <?php dynamic_sidebar('custom_sidebar_2'); ?>
      </div><!-- #custom-sidebar-2 -->
    <?php endif; ?>

    <div class="clearfix">

      <div class="ins-chevron">
        <div class="ins-chevron-inner">
          <h4 class="ins-chevron-title">Delivery</h4>
          <p class="ins-chevron-body">Delivering your flooring ahead of time allows wood to acclimate for proper moisture level.</p>
        </div>
      </div>

      <div class="ins-chevron">
        <div class="ins-chevron-inner">
          <h4 class="ins-chevron-title">Preparation</h4>
          <p class="ins-chevron-body">Hang plastic to mask off rooms, move appliances, remove trim.</p>
        </div>
      </div>

      <div class="ins-chevron">
        <div class="ins-chevron-inner">
          <h4 class="ins-chevron-title">Demolition</h4>
          <p class="ins-chevron-body">Removing tile, carpet, vinyl, & laminate.</p>
        </div>
      </div>

      <div class="ins-chevron">
        <div class="ins-chevron-inner">
          <h4 class="ins-chevron-title">Sub-Floor Prep</h4>
          <p class="ins-chevron-body">Sanding seams, leveling, fixing squeaky spots.</p>
        </div>
      </div>

      <div class="ins-chevron">
        <div class="ins-chevron-inner">
          <h4 class="ins-chevron-title">Install</h4>
          <p class="ins-chevron-body">Trained employees will install floor according to NWFA standards.</p>
        </div>
      </div>

      <div class="ins-chevron">
        <div class="ins-chevron-inner">
          <h4 class="ins-chevron-title">Finish</h4>
          <p class="ins-chevron-body">Move appliances back, Haul away trash, Post Construction Clean.</p>
        </div>
      </div>

      <div class="ins-chevron">
        <div class="ins-chevron-inner">
          <h4 class="ins-chevron-title">Clean Up & Walkthrough</h4>
          <p class="ins-chevron-body">John or Ben will complete walk-through and deliver cleaning kit/maintenance brochure</p>
        </div>
      </div>

    </div>

  </div>
</section>

<section id="gallery-section">
	<div class="border"></div><!--end funnel-border-->
		<div id="desktop-gallery">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home - Gallery") ) : ?><?php endif; ?>
		</div><!--end desktop-gallery-->
		<div id="mobile-gallery">
			<a href="/gallery/" class="button">Wood Floor Gallery</a>
		</div><!--end mobile-gallery-->
	<div class="border"></div><!--end funnel-border-->
</section><!--end gallery-section-->

<section id="testimonials" class="row">
	<div class="inner">
      <?php echo do_shortcode('[hms_testimonials_rotating group="1" template="1" order="random"]'); ?>
	</div><!--end inner-->
</section><!--end testimonials-->

<div id="contact-form" class="row">
	<div class="inner">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Contact Form") ) : ?><?php endif; ?>
	</div><!--end inner-->
</div><!--end contact-form-->
<?php get_footer(); ?>
