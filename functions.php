<?php
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//

require_once('lib/helpers.php');
require_once('lib/shortcodes.php');

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
		$style = lc_helper_enqueue('style.css');
		$script = lc_helper_enqueue('js/main.js');

    // wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
		wp_enqueue_style('child-style',
        $style['uri'],
        array(
					// 'parent-style',
				),
				$style['file_last_mod']
    );

		wp_enqueue_script('child-script',
        $script['uri'],
        array('jquery'),
				$script['file_last_mod']
    );
}

register_sidebar(
            array(
            'name' => 'Header Phone',
            'id' => 'header-phone',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));


register_sidebar(
            array(
            'name' => 'Home - Hero',
            'id' => 'hero',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Home - Featured',
            'id' => 'home-featured',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Funnel - Refinishing',
            'id' => 'funnel-refinishing',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Funnel - Maintenance',
            'id' => 'funnel-maintenance',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Funnel - Clean',
            'id' => 'funnel-clean',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Right Sidebar',
            'id' => 'right-sidebar',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Cleaning - Intensive Image',
            'id' => 'cleaning-intensive-image',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Cleaning - Intensive Description',
            'id' => 'cleaning-intensive-description',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Cleaning - Tykote Image',
            'id' => 'cleaning-tykote-image',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Cleaning - Tykote Description',
            'id' => 'cleaning-tykote-description',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Maintenance Schedule',
            'id' => 'maintenance-schedule',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Our Mission',
            'id' => 'our-mission',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h2><span>',
            'after_title' => '</span></h2>',
));

register_sidebar(
            array(
            'name' => 'Home - Gallery',
            'id' => 'home-gallery',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Testimonials',
            'id' => 'testimonials',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h2>',
            'after_title' => '</h2>',
));

register_sidebar(
            array(
            'name' => 'Contact Form',
            'id' => 'contact-form',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h2>',
            'after_title' => '</h2>',
));

register_sidebar(
            array(
            'name' => 'Footer Logo',
            'id' => 'footer-logo',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Footer Cities',
            'id' => 'footer-cities',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Footer Right',
            'id' => 'footer-right',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
            array(
            'name' => 'Copyright',
            'id' => 'copyright',
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '<h3>',
            'after_title' => '</h3>',
));

register_sidebar(
  array(
		'name' => 'Full-Width Page Video Header',
		'id' => 'tpl-full-width-video-header',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<!-- ',
		'after_title' => ' -->',
));

register_sidebar(
  array(
		'name' => 'Full-Width Page Template Top',
		'id' => 'tpl-full-width-top',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<!--',
		'after_title' => ' -->',
));

register_sidebar(
  array(
		'name' => 'Full-Width Page Template Bottom',
		'id' => 'tpl-full-width-bottom',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<!-- ',
		'after_title' => ' -->',
));

// Installation page
register_sidebar(array(
  'name'          => 'Installation Galleries',
  // 'description'   => 'Description.',
  'id'            => 'custom_sidebar_1',
  'before_widget' => '<div class="col-xs-12 col-sm-6 col-md-3">',
  'after_widget'  => '</div>',
  'before_title'  => '<h4 class="ins-card-title">',
  'after_title'   => '</h4>',
));

register_sidebar(array(
  'name'          => 'Installation Points',
  // 'description'   => 'Description.',
  'id'            => 'custom_sidebar_2',
  'before_widget' => '',
  'after_widget'  => '',
  'before_title'  => '<h4>',
  'after_title'   => '</h4>',
));

register_sidebar(array(
  'name'          => 'Installation Heder Caption',
  // 'description'   => 'Description.',
  'id'            => 'custom_sidebar_3',
  'before_widget' => '',
  'after_widget'  => '',
  'before_title'  => '<h4>',
  'after_title'   => '</h4>',
));

// Remove widget titles
function lc_remove_widget_title($widget_title) {
  return substr($widget_title, 0, 1) == "!" ? "" : $widget_title;
}
add_filter('widget_title', 'lc_remove_widget_title');

// Replaces the excerpt "Read More" text by a link
function new_excerpt_more($more) {
       global $post;
	return ' <a class="moretag" href="'. get_permalink($post->ID) . '">Read More</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');
