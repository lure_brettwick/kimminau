<?php if (!defined('ABSPATH')) exit; ?>

<div id="lc-image-block-<?php echo $id; ?>" class="lc-image-block lc-image-block--<?php echo $position; ?>">
  <div class="lc-image-block__image">
    <div class="lc-image-block__image__background" style="background-image: url(<?php echo $src; ?>);"></div>
  </div>
  <div class="lc-image-block__content">
    <div class="lc-image-block__content__inner">
      <?php echo do_shortcode($content); ?>
    </div>
  </div>
</div>
