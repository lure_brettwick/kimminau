<?php if (!defined('ABSPATH')) exit; ?>

<div class="lc-accordion-panel panel panel-default">
  <div class="panel-heading" role="tab" id="heading-<?php echo $id; ?>">
    <h4 class="panel-title">
      <a role="button"
				<?php if (!$is_open) { echo 'class="collapsed"'; } ?>
				data-toggle="collapse"
				<?php if ($parent) { echo "data-parent='$parent'"; } ?>
				href="#collapse-<?php echo $id; ?>"
				<?php echo 'aria-expanded="' . ( $is_open ? "true" : "false") . '"'; ?>
				aria-controls="collapse-<?php echo $id; ?>">
				<?php if ($icon) : ?>
					<span class="lc-icon lc-icon--<?php echo $icon; ?>"></span>
				<?php endif; ?>
				<span class="lc-collapse-icon glyphicon glyphicon-chevron-down"></span>
        <?php echo $title; ?>
      </a>
    </h4>
  </div>
  <div id="collapse-<?php echo $id; ?>" class="panel-collapse collapse <?php if ($is_open) { echo 'in'; } ?>"
		<?php echo 'aria-expanded="' . ( $is_open ? "true" : "false") . '"'; ?>
		role="tabpanel" aria-labelledby="heading-<?php echo $id; ?>">
    <div class="panel-body">
      <?php echo do_shortcode($content); ?>
    </div>
  </div>
</div>
