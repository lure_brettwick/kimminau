<?php if (!defined('ABSPATH')) exit; ?>

<section id="<?php echo $id; ?>" class="lc-section <?php echo $add_class; ?>" style="min-height: <?php echo $min_height; ?>">
  <div <?php if ($inner_container) { echo 'class="container"'; } ?>>
    <?php echo do_shortcode($content); ?>
  </div>
</section>
