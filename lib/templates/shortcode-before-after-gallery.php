<?php if (!defined('ABSPATH')) exit; ?>

<div class="lc-refinishing-gallery-block">
	<div class="border"></div>
	<div class="row">
		<div class="col-sm-6">
			<a href="<?php echo $href; ?>" target="<?php echo $target ?>">
				<img src="<?php echo $image_left; ?>" alt="" />
			</a>
			<div class="lc-refinishing-gallery-block__caption row">
				<div class="col-xs-6 text-center text-serif text-white text-shadow text-21px">Before</div>
				<div class="col-xs-6 text-center text-serif text-white text-shadow text-21px">After</div>
			</div>
		</div>
		<div class="col-sm-6">
			<a href="<?php echo $href; ?>" target="<?php echo $target ?>">
				<img src="<?php echo $image_right; ?>" alt="" />
			</a>
			<div class="lc-refinishing-gallery-block__caption row">
				<div class="col-xs-6 text-center text-serif text-white text-shadow text-21px">Before</div>
				<div class="col-xs-6 text-center text-serif text-white text-shadow text-21px">After</div>
			</div>
		</div>
	</div>
	<div class="content">
		<a class="btn btn-serif btn-serif--transparent" href="<?php echo $href; ?>" target="<?php echo $target ?>">
			<?php echo $content; ?>
		</a>
	</div>
	<div class="border"></div>
</div>
