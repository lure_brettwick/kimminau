<?php

if (!function_exists('lc_shortcode_empty_paragraph_fix')) {

	add_filter('the_content', 'lc_shortcode_empty_paragraph_fix');

	function lc_shortcode_empty_paragraph_fix( $content ) {
    $array = array(
      '<p>['    => '[',
      ']</p>'   => ']',
      ']<br />' => ']'
    );
    return strtr($content, $array);
	}
}

if (!function_exists('lc_helper_enqueue')) {

	function lc_helper_enqueue($file_path = null) {
		$file_path_uri = get_stylesheet_directory_uri() . '/' . $file_path;
		$file_last_mod = filemtime(get_stylesheet_directory() . '/' . $file_path);

		return array(
			'uri' => $file_path_uri,
			'file_last_mod' => $file_last_mod,
		);
	}
}
