<?php

if (!function_exists('lc_shortcode_accordion')) {

	add_shortcode('lc_accordion', 'lc_shortcode_accordion');

	function lc_shortcode_accordion($atts = array(), $content = null) {
		extract(shortcode_atts(array(
			'id' => uniqid(),
			'title' => '',
			'parent' => '',
			'icon' => '',
			'is_open' => false,
		), $atts));

		ob_start();
		include(locate_template('lib/templates/shortcode-accordion.php'));
		return ob_get_clean();
	}
}

if (!function_exists('lc_shortcode_section')) {

	add_shortcode('lc_section', 'lc_shortcode_section');

	function lc_shortcode_section($atts = array(), $content = null) {
		extract(shortcode_atts(array(
			'id' => 'lc-section-' . uniqid(),
			'add_class' => '',
			'min_height' => '0',
			'inner_container' => false,
		), $atts));

		ob_start();
		include(locate_template('lib/templates/shortcode-section.php'));
		return ob_get_clean();
	}
}

if (!function_exists('lc_shortcode_image_block')) {

	add_shortcode('lc_image_block', 'lc_shortcode_image_block');

	function lc_shortcode_image_block($atts = array(), $content = null) {
		extract(shortcode_atts(array(
			'id' => 'lc-image-block-' . uniqid(),
			'src' => '',
			'position' => 'left',
		), $atts));

		ob_start();
		include(locate_template('lib/templates/shortcode-image-block.php'));
		return ob_get_clean();
	}
}

if (!function_exists('lc_shortcode_before_after_gallery')) {

	add_shortcode('lc_before_after_gallery', 'lc_shortcode_before_after_gallery');

	function lc_shortcode_before_after_gallery($atts = array(), $content = null) {
		extract(shortcode_atts(array(
			'href' => '#',
			'image_left' => '/wp-content/themes/kimminau/img/gallery-refinishing-before-after-1.jpg',
			'image_right' => '/wp-content/themes/kimminau/img/gallery-refinishing-before-after-2.jpg',
		), $atts));

		ob_start();
		include(locate_template('lib/templates/shortcode-before-after-gallery.php'));
		return ob_get_clean();
	}
}
