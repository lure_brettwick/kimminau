<?php

/*
Template Name: Clean & Coat Page
*/

get_header(); ?>
<?php get_template_part('template-part', 'head'); ?>
<!-- start content container -->
<?php // theloop
        if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	$image = $image[0]; ?>
	<?php else :
	$image = get_bloginfo( 'stylesheet_directory') . '/img/default-header-bg.jpg'; ?>
	<?php endif; ?>
<div id="header-bg" class="row" style="background-image: url('<?php echo $image; ?>')">
	<div class="inner">
		<h1 class="page-header"><?php the_title() ;?></h1>
	</div><!--end inner-->
</div><!--end header-bg-->
<div class="border"></div><!--end border-->
<div class="row dmbs-content">
   <div class="inner">
	<div id="content" class="col-xs-12 col-sm-8">
        <?php the_content(); ?>
        <?php endwhile; ?>
        <?php else: ?>
            <?php get_404_template(); ?>
        <?php endif; ?>
	</div><!--end content-->
	<div id="right-sidebar" class="col-xs-12 col-sm-4">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Right Sidebar") ) : ?><?php endif; ?>
	</div><!--end right-sidebar-->
   </div><!--end inner-->
</div>
<div id="cleaning-section" class="row">
	<div class="inner">
		<div id="cleaning" class="col-xs-12 col-sm-8">
			<div id="intensive" class="row">
				<div class="image col-xs-12 col-sm-4">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Cleaning - Intensive Image") ) : ?><?php endif; ?>
				</div><!--end image-->
				<div class="description col-xs-12 col-sm-8">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Cleaning - Intensive Description") ) : ?><?php endif; ?>
				</div><!--end description-->
			</div><!--end intensive-->
			<div id="tykote" class="row">
				<div class="image col-xs-12 col-sm-4">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Cleaning - Tykote Image") ) : ?><?php endif; ?>
				</div><!--end image-->
				<div class="description col-xs-12 col-sm-8">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Cleaning - Tykote Description") ) : ?><?php endif; ?>
				</div><!--end description-->
			</div><!--end tykote-->
		</div><!--end cleaning-->
		<div class="col-xs-12 col-sm-4 maintenance">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Maintenance Schedule") ) : ?><?php endif; ?>
		</div><!--end maintenance-->
	</div><!--end inner-->
</div><!--end cleaning-section-->
<!-- end content container -->
<div id="gallery-section">
	<div class="border"></div><!--end funnel-border-->
		<div id="desktop-gallery">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home - Gallery") ) : ?><?php endif; ?>
		</div><!--end desktop-gallery-->
		<div id="mobile-gallery">
			<a href="/gallery/" class="button">Wood Floor Gallery</a>
		</div><!--end mobile-gallery-->
	<div class="border"></div><!--end funnel-border-->
</div><!--end gallery-section-->
<div id="contact-form" class="row">
	<div class="inner">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Contact Form") ) : ?><?php endif; ?>
	</div><!--end inner-->
</div><!--end contact-form-->
<div class="border"></div><!--end funnel-border-->
<div id="testimonials" class="row secondary">
	<div class="inner">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Testimonials") ) : ?><?php endif; ?>
			<a href="/testimonials/" class="button">More Testimonials</a>
	</div><!--end inner-->
</div><!--end testimonials-->
<?php get_footer(); ?>
