<?php get_header(); ?>
<?php get_template_part('template-part', 'head'); ?>
<?php get_template_part('template-part', 'topnav'); ?>
<!-- start content container -->
<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	$image = $image[0]; ?>
	<?php else :
	$image = get_bloginfo( 'stylesheet_directory') . '/img/default-header-bg.jpg'; ?>
	<?php endif; ?>
<div id="header-bg" class="row" style="background-image: url('<?php echo $image; ?>')">
	<div class="inner">
		<h1 class="page-header"><?php the_title() ;?></h1>
	</div><!--end inner-->
</div><!--end header-bg-->
<div class="border"></div><!--end border-->
<div class="row dmbs-content">
   <div class="inner">
	<div id="content" class="col-xs-12 col-sm-9">
		<?php // theloop
                if ( have_posts() ) : while ( have_posts() ) : the_post();

                    // single post
                    if ( is_single() ) : ?>

                        <div <?php post_class(); ?>>
			    <div class="post-data">Posted on <?php the_date(); ?> by <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="author"><?php the_author(); ?></a></div>
                            <?php the_content(); ?>
                            <?php wp_link_pages(); ?>
                        

                        </div>
                    <?php
                    // list of posts
                    else : ?>
                       <div <?php post_class(); ?>>

                            <h2 class="page-header">
                                <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'devdmbootstrap3' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                            </h2>
                            <?php the_content(); ?>
                            <?php wp_link_pages(); ?>
                            <?php get_template_part('template-part', 'postmeta'); ?>
                            <?php  if ( comments_open() ) : ?>
                                   <div class="clear"></div>
                                  <p class="text-right">
                                      <a class="btn btn-success" href="<?php the_permalink(); ?>#comments"><?php comments_number(__('Leave a Comment','devdmbootstrap3'), __('One Comment','devdmbootstrap3'), '%' . __(' Comments','devdmbootstrap3') );?> <span class="glyphicon glyphicon-comment"></span></a>
                                  </p>
                            <?php endif; ?>
                       </div>

                     <?php  endif; ?>

                <?php endwhile; ?>
                  <?php posts_nav_link(); ?>
                <?php else: ?>

                    <?php get_404_template(); ?>

            <?php endif; ?>

	</div><!--end content-->
	<div id="right-sidebar" class="col-xs-12 col-sm-3">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Right Sidebar") ) : ?><?php endif; ?>
	</div><!--end home-featured-->
   </div><!--end inner-->
</div>
<!-- end content container -->
<?php get_footer(); ?>